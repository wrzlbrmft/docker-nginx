# docker-nginx

Customized NGINX

Based on https://hub.docker.com/_/nginx . Upgraded and with some extra packages.

```
docker pull wrzlbrmft/nginx:latest
```

See also:

  * https://nginx.org/
  * https://hub.docker.com/r/wrzlbrmft/nginx/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
