ARG TAG=alpine
FROM nginx:${TAG}

RUN apk upgrade --no-cache \
    && apk add --no-cache \
        netcat-openbsd
